const map = (elements, cb) => {
  
  if (!cb || !elements || !Array.isArray(elements) || typeof cb !== "function")
    return [];

  const toReturn = [];
  for (let i = 0; i < elements.length; i++) {
    toReturn.push(cb(elements[i], i, elements));
  }
  return toReturn;
};

module.exports = map;

