const find = (elements, cb) => {
  
  if (!cb || !elements || !Array.isArray(elements) || typeof cb !== "function")
    return;

  for (let i = 0; i < elements.length; i++) {
    if (cb(elements[i], i, elements)) {
      return elements[i];
    }
  }
};

module.exports = find;

