const reduce = (elements, cb, startingValue) => {
  
  if (!cb || !elements || !Array.isArray(elements) || typeof cb !== "function")
    return;

  let i = 0;

  if (!startingValue) {
    startingValue = elements[0];
    i++;
  }

  for (; i < elements.length; i++) {
    startingValue = cb(startingValue, elements[i], i, elements);
  }
  return startingValue;
};

module.exports = reduce;

