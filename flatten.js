const flatten = (elements, depth = 1) => {
  
  if (!elements || !Array.isArray(elements)) return [];

  const toReturn = [];

  for (let i = 0; i < elements.length; i++) {
    if (Array.isArray(elements[i]) && depth > 0) {
      toReturn.push(...flatten(elements[i], depth - 1));
    } else {
      toReturn.push(elements[i]);
    }
  }

  return toReturn;
};

module.exports = flatten;

