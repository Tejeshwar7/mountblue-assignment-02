const each = (elements, cb) => {

  if (!cb || !elements || !Array.isArray(elements) || typeof cb !== "function")
    return;

  for (let i = 0; i < elements.length; i++) {
    cb(elements[i], i, elements);
  }
};

module.exports = each;

