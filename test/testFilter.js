const  array  = require("../array");
const filter = require("../filter");

const cb = (elements) => elements % 3 === 0;

const result = filter(array, cb);

console.log(result);

