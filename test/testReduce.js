const  array  = require("../array");
const reduce = require("../reduce");

const cb = (accumulator, value) => accumulator + value;

const result = reduce(array, cb, 5);

console.log(result);

