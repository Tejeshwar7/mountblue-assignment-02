const filter = (elements, cb) => {

  if (!cb || !elements || !Array.isArray(elements) || typeof cb !== "function")
    return [];

  let toReturn = [];

  for (let i = 0; i < elements.length; i++) {
    if (cb(elements[i], i, elements)) {
      toReturn.push(elements[i]);
    }
  }
  return toReturn;
};

module.exports = filter;

